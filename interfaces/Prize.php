<?php
namespace app\interfaces;
/**
 * Created by PhpStorm.
 * User: aiv_dev
 * Date: 26.02.18
 * Time: 23:26
 */

interface Prize
{
    public function generatePrize(): void;
    public function getPrizeCongratulations(): string;
    public function getPrizeValue();
    public function getPrize();
    public function setPrize(int $value);
}