<?php
namespace app\controllers;

use app\classes\MyPrize;
use app\models\CashInBank;
use app\models\LoyaltyPointsUser;
use app\models\UserPostBox;
use app\models\UserPrizeSearch;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\ContactForm;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'signup',
                            'login',
                            'request-password-reset',
                            'reset-password'
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'logout',
                            'index',
                            'create',
                            'convert-cash',
                            'cancel-prize',
                            'award-prize'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists my UserPrize models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;
        $searchModel = new UserPrizeSearch();
        $dataProvider = $searchModel->search($queryParams);

        $bankAccount = new CashInBank();
        $loyaltyPointAccount = new LoyaltyPointsUser();
        $postBox = new UserPostBox();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'bankAccount' => $bankAccount,
            'loyaltyPointAccount' => $loyaltyPointAccount,
            'postBox' => $postBox,
        ]);
    }

    /**
     * Creates a new UserPrize model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $myNewPrize = MyPrize::getMyPrize();

        if(!$myNewPrize){
            Yii::error("SavePrizeError");
            return [
                'msg' => 'Ошибка создания',
            ];
        }
        return [
            'ok' => true,
            'msg' => $myNewPrize->getPrize()->getPrizeCongratulations(),
        ];
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionConvertCash(int $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $myNewPrize = MyPrize::getMyPrize($id);
        if(!$myNewPrize || !$myNewPrize->exchangeMoneyToPoints()){
            Yii::error("ConvertCashError");
            return [
                'msg' => 'Ошибка конвертации',
            ];
        }
        return [
            'ok' => true,
            'msg' => 'Conversation is success!',
        ];
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCancelPrize(int $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var int $id */
        $myNewPrize = MyPrize::getMyPrize($id);
        if(!$myNewPrize || !$myNewPrize->cancelPrize()){
            Yii::error("Prize cancel error");
            return [
                'msg' => 'Prize cancel error',
            ];
        }
        return [
            'ok' => true,
            'msg' => 'Prize is canceled!',
        ];
    }

    /**
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionAwardPrize(int $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $myNewPrize = MyPrize::getMyPrize($id);
        if(!$myNewPrize || !$myNewPrize->prizeToAward()){
            Yii::error("Prize award error");
            return [
                'msg' => 'Prize award error',
            ];
        }
        return [
            'ok' => true,
            'msg' => 'Award in process!',
        ];
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
