<?php
/**
 * Created by PhpStorm.
 * User: aiv_dev
 * Date: 09.11.17
 * Time: 11:00
 */

namespace app\helpers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

class LinkPagerShowAllButton extends LinkPager
{
    const SHOW_ALL = 'all';

    protected function renderPageButtons()
    {
        $renderedLeftButton = parent::renderPageButtons();
        $rightButton = null;

        if ($renderedLeftButton) {
            $rightButton = $this->renderRightPageButton('Show all', self::SHOW_ALL);
        } elseif ($this->pagination->totalCount >= $this->pagination->pageSize) {
            $rightButton = $this->renderRightPageButton('To pages', 1);
        }

        $renderedRightButton = $rightButton ? Html::tag('ul', $rightButton, ['class' => 'pagination pull-right']) : $rightButton;

        return $renderedLeftButton . $renderedRightButton;
    }

    protected function renderRightPageButton($label, $page)
    {
        $options = ['class' => $this->pageCssClass];
        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;

        return Html::tag('li', Html::a($label, Url::current(['page' => $page]), $linkOptions), $options);
    }
}
