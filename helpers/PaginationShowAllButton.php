<?php
/**
 * Created by PhpStorm.
 * User: aiv_dev
 * Date: 09.11.17
 * Time: 13:37
 */

namespace app\helpers;

use yii\data\Pagination;

class PaginationShowAllButton extends Pagination
{
    const SHOW_ALL = 'all';

    public function getPage($recalculate = false)
    {
        $page = $this->getQueryParam($this->pageParam, 1);
        if ($page == self::SHOW_ALL){
            $totalCount = $this->totalCount;
            $this->defaultPageSize = $totalCount;
            $this->setPageSize(0);
        }

        return parent::getPage($recalculate);
    }

    public function getPageSize()
    {
        $page = $this->getQueryParam($this->pageParam, 1);
        if ($page == self::SHOW_ALL) {
            return $this->totalCount;
        }

        return parent::getPageSize();
    }
}
