$('#createPrizeButton').click(function () {
    var url = 'index.php?r=site/create';
    $.get(url, function (data, status) {
        if (data.ok) {
            $('#alertMsg').html(data.msg);
            $('#alertMsg').show();
            $.pjax.reload({container: '#myPrizesList'}).done(function () {
                $.pjax.reload({container: '#leftoverPrize'});
            });
            setTimeout(function () {
                $('#alertMsg').hide();
                $('#alertMsg').html('');
            }, 2000);
        }
    });
});

convertCashToPoint = function (id) {
    var url = 'index.php?r=site/convert-cash&id=' + id;
    $.get(url, function (data, status) {
        if (data.ok) {
            $('#alertMsg').html(data.msg);
            $('#alertMsg').show();
            $.pjax.reload({container: '#myPrizesList'}).done(function () {
                $.pjax.reload({container: '#myPanels'}).done(function () {
                    $.pjax.reload({container: '#leftoverPrize'});
                });
            });
            setTimeout(function () {
                $('#alertMsg').hide();
                $('#alertMsg').html('');
            }, 2000);
        }
    });
};

awardPrize = function (id) {
    var url = 'index.php?r=site/award-prize&id=' + id;
    $.get(url, function (data, status) {
        if (data.ok) {
            $('#alertMsg').html(data.msg);
            $('#alertMsg').show();
            $.pjax.reload({container: '#myPrizesList'}).done(function () {
                $.pjax.reload({container: '#myPanels'});
            });
            setTimeout(function () {
                $('#alertMsg').hide();
                $('#alertMsg').html('');
            }, 2000);
        }
    });
};

cancelPrize = function (id) {
    var url = 'index.php?r=site/cancel-prize&id=' + id;
    $.get(url, function (data, status) {
        if (data.ok) {
            $('#alertMsg').html(data.msg);
            $('#alertMsg').show();
            $.pjax.reload({container: '#myPrizesList'}).done(function () {
                $.pjax.reload({container: '#leftoverPrize'});
            });
            setTimeout(function () {
                $('#alertMsg').hide();
                $('#alertMsg').html('');
            }, 2000);
        }
    });
};