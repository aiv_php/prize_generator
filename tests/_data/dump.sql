-- Adminer 4.5.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `user_prize`;
CREATE TABLE `user_prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `value` int(11) DEFAULT NULL,
  `awarded` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx-user_prize-user_id` (`user_id`),
  KEY `idx-user_prize-type_id` (`type_id`),
  CONSTRAINT `fk-user_prize-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user_prize` (`id`, `user_id`, `type_id`, `value`, `awarded`) VALUES
(1,	1,	1,	450,	0),
(2,	1,	1,	200,	1),
(3,	1,	1,	150,	2),
(4,	1,	1,	250,	3),
(5,	1,	1,	0,	1),
(6,	1,	2,	1,	0);


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1,	'test',	'fCpFF_8J4iC5_lcHnR0LT1PIatsY0HMZ',	'$2y$13$GuTTAC33EMNJkbA65GGRU.P2ajoGc5YCabnSEowd0yqUDTbJFd4CC',	NULL,	'test@test.te',	10,	1519677922,	1519677922);

-- 2018-03-06 08:58:13