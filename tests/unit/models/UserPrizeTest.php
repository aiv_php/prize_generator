<?php

namespace tests\models;

use app\models\User;
use app\models\UserPrize;
use Codeception\Test\Unit;
use Yii;

class UserPrizeTest extends Unit
{
    public function testExchangeMoneyToPointsWithCurrentData()
    {
        expect_that($userPrize = UserPrize::findOne(1));
        expect_that($user = User::findIdentity(1));
        Yii::$app->user->login($user, 60);
        expect($userPrize->exchangeMoneyToPoints())->equals(true);
    }

    public function testExchangeMoneyToPointsWithWrongUser()
    {
        expect_that($userPrize = UserPrize::findOne(2));
        expect_that($user = User::findIdentity(1));
        Yii::$app->user->login($user, 60);
        expect($userPrize->exchangeMoneyToPoints())->equals(false);
    }

    public function testExchangeMoneyToPointsWithWrongType()
    {
        expect_that($userPrize = UserPrize::findOne(3));
        expect_that($user = User::findIdentity(1));
        Yii::$app->user->login($user, 60);
        expect($userPrize->exchangeMoneyToPoints())->equals(false);
    }

    public function testExchangeMoneyToPointsWithWrongAwardParameter()
    {
        expect_that($userPrize = UserPrize::findOne(4));
        expect_that($user = User::findIdentity(1));
        Yii::$app->user->login($user, 60);
        expect($userPrize->exchangeMoneyToPoints())->equals(false);
    }

    public function testExchangeMoneyToPointsWithZeroPrize()
    {
        expect_that($userPrize = UserPrize::findOne(5));
        expect_that($user = User::findIdentity(1));
        Yii::$app->user->login($user, 60);
        expect($userPrize->exchangeMoneyToPoints())->equals(true);
    }
}
