<?php

use yii\db\Migration;

/**
 * Class m180303_220544_CreateCashInBankTable
 */
class m180303_220544_CreateCashInBankTable extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%cash_in_bank}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'amount' => $this->double(2)->null(),
        ], $tableOptions);

        $this->createIndex(
            'idx-cash_in_bank-user_id',
            '{{%cash_in_bank}}',
            'user_id'
        );

        $this->addForeignKey(
            'fk-cash_in_bank-user_id',
            '{{%cash_in_bank}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {

        $this->dropForeignKey(
            'fk-cash_in_bank-user_id',
            '{{%cash_in_bank}}'
        );

        $this->dropIndex(
            'idx-cash_in_bank-user_id',
            '{{%cash_in_bank}}'
        );

        $this->dropTable('{{%cash_in_bank}}');
    }
}
