<?php

use yii\db\Migration;

/**
 * Class m180305_025151_AddAvailableAmountToCashTable
 */
class m180305_025151_AddAvailableAmountToCashTable extends Migration
{
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%availables}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'available_amount' => $this->float(2)->null(),
        ], $tableOptions);

        $this->batchInsert('availables', ['name', 'available_amount'], [['cash', '200']]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%availables}}');
    }
}
