<?php

use yii\db\Migration;

/**
 * Class m180304_175951_CreateUserPostBoxTable
 */
class m180304_175951_CreateUserPostBoxTable extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_post_box}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'present_id' => $this->integer()->notNull(),
            'amount' => $this->integer()->null(),
        ], $tableOptions);

        $this->createIndex(
            'idx-user_post_box-user_id',
            '{{%user_post_box}}',
            'user_id'
        );

        $this->createIndex(
            'idx-user_post_box-present_id',
            '{{%user_post_box}}',
            'present_id'
        );

        $this->addForeignKey(
            'fk-user_post_box-user_id',
            '{{%user_post_box}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-user_post_box-present_id',
            '{{%user_post_box}}',
            'present_id',
            '{{%present}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {

        $this->dropForeignKey(
            'fk-user_post_box-present_id',
            '{{%user_post_box}}'
        );

        $this->dropForeignKey(
            'fk-user_post_box-user_id',
            '{{%user_post_box}}'
        );

        $this->dropIndex(
            'idx-user_post_box-present_id',
            '{{%user_post_box}}'
        );

        $this->dropIndex(
            'idx-user_post_box-user_id',
            '{{%user_post_box}}'
        );

        $this->dropTable('{{%user_post_box}}');
    }
}
