<?php

use yii\db\Migration;

/**
 * Class m180226_205731_CreatePresentTable
 */
class m180226_205731_CreatePresentTable extends Migration
{
    /**
     * @return bool|void
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%present}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'available_quantity' => $this->integer()->defaultValue(10),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%present}}');
    }
}
