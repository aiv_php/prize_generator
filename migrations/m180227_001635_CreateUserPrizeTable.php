<?php

use yii\db\Migration;

/**
 * Class m180227_001635_CreateUserPrizeTable
 */
class m180227_001635_CreateUserPrizeTable extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_prize}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'value' => $this->integer()->null(),
            'awarded' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex(
            'idx-user_prize-user_id',
            '{{%user_prize}}',
            'user_id'
        );

        $this->createIndex(
            'idx-user_prize-type_id',
            '{{%user_prize}}',
            'type_id'
        );

        $this->addForeignKey(
            'fk-user_prize-user_id',
            '{{%user_prize}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {

        $this->dropForeignKey(
            'fk-user_prize-type_id',
            '{{%user_prize}}'
        );

        $this->dropIndex(
            'idx-user_prize-type_id',
            '{{%user_prize}}'
        );

        $this->dropIndex(
            'idx-user_prize-user_id',
            '{{%user_prize}}'
        );

        $this->dropTable('{{%user_prize}}');
    }
}
