<?php

use yii\db\Migration;

/**
 * Class m180226_214625_CreateLoyaltyPointsUserTable
 */
class m180226_214625_CreateLoyaltyPointsUserTable extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%loyalty_points_user}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'amount' => $this->integer()->null(),
        ], $tableOptions);

        $this->createIndex(
            'idx-loyalty_points_user-user_id',
            '{{%loyalty_points_user}}',
            'user_id'
        );

        $this->addForeignKey(
            'fk-loyalty_points_user-user_id',
            '{{%loyalty_points_user}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {

        $this->dropForeignKey(
            'fk-loyalty_points_user-user_id',
            '{{%loyalty_points_user}}'
        );

        $this->dropIndex(
            'idx-loyalty_points_user-user_id',
            '{{%loyalty_points_user}}'
        );

        $this->dropTable('{{%loyalty_points_user}}');
    }
}
