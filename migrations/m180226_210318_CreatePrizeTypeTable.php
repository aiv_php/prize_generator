<?php

use yii\db\Migration;

/**
 * Class m180226_210318_CreatePrizeTypeTable
 */
class m180226_210318_CreatePrizeTypeTable extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%prize_type}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(100)->notNull()->unique(),
            'deleted' => $this->smallInteger(1)->null(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%prize_type}}');
    }
}
