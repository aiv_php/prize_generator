<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cash_in_bank".
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 *
 * @property User $user
 */
class CashInBank extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_in_bank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['amount'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => 'User ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param $userId
     * @return CashInBank
     */
    public function getUserCash($userId): CashInBank
    {
        $userCash = self::findOne(['user_id' => $userId]);

        if(!$userCash){
            $userCash = new CashInBank();
            $userCash->user_id = $userId;
            $userCash->save();
        }

        return $userCash;
    }

    /**
     * @param $userId
     * @return float|null
     */
    public function getCashForUser($userId): float
    {
        $cash = $this->getUserCash($userId)->amount;

        return $cash ?? 0.00;
    }

    /**
     * @return float|null
     */
    public function getMyCash()
    {
        if (!empty(Yii::$app->user)) {
            $iAm = Yii::$app->user->id;

            return $this->getCashForUser($iAm);
        }

        return null;
    }

    /**
     * @param int $userId
     * @param float $amount
     * @return bool
     */
    public function userAccountReplenishment(int $userId, float $amount)
    {
        $userCash = $this->getUserCash($userId);
        $userCash->amount += $amount;

        return $userCash->save();
    }
}
