<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserPrize;

/**
 * UserPrizeSearch represents the model behind the search form of `app\models\UserPrize`.
 */
class UserPrizeSearch extends UserPrize
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'type_id', 'value', 'awarded'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {

        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $identityUser = Yii::$app->user;

        $query = UserPrize::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['id']]

        ]);
        $dataProvider->pagination->pageSize = 6;

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $identityUser->id,
            'type_id' => $this->type_id,
            'awarded' => $this->awarded,
        ]);

        $query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
}
