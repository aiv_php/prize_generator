<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loyalty_points_user".
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 *
 * @property User $user
 */
class LoyaltyPointsUser extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loyalty_points_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'amount'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => 'User ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param int $userId
     * @return LoyaltyPointsUser
     */
    public function getUserLoyaltyPoints(int $userId): LoyaltyPointsUser
    {
        $userLoyaltyPoints = self::findOne(['user_id' => $userId]);

        if(!$userLoyaltyPoints){
            $userLoyaltyPoints = new LoyaltyPointsUser();
            $userLoyaltyPoints->user_id = $userId;
            $userLoyaltyPoints->save();
        }

        return $userLoyaltyPoints;
    }

    /**
     * @param $userId
     * @return int
     */
    public function getLoyaltyPointsForUser($userId): int
    {
        $loyaltyPoints = $this->getUserLoyaltyPoints($userId)->amount;
        return $loyaltyPoints ?? 0;
    }

    /**
     * @return int|null
     */
    public function getMyLoyaltyPoints()
    {
        if (!empty(Yii::$app->user)) {
            $iAm = Yii::$app->user->id;
            return $this->getLoyaltyPointsForUser($iAm);
        }
    }

    /**
     * @param int $userId
     * @param int $amount
     * @return bool
     */
    public function userAccountReplenishment(int $userId, int $amount)
    {
        $userLoyaltyPoints = $this->getUserLoyaltyPoints($userId);
        $userLoyaltyPoints->amount += $amount;
        return $userLoyaltyPoints->save();
    }
}
