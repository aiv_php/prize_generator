<?php

namespace app\models;

use app\classes\CashPrize;
use app\classes\LoyalityPointsPrize;
use app\classes\PresentPrize;
use app\interfaces\Prize;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "user_prize".
 *
 * @property int $id
 * @property int $user_id
 * @property int $type_id
 * @property int $value
 * @property int $awarded
 *
 * @property User $user
 */
class UserPrize extends ActiveRecord
{

    const CASH_PRIZE = 1;
    const PRESENT_PRIZE = 2;
    const LOYALITY_POINTS_PRIZE = 3;

    const READY_TO_AWARDING = 0;
    const IN_PROCESS_AWARDING = 1;
    const AWARDED = 2;
    const CANCELED = 3;

    const EXCHANGE_MONEY_TO_LOYALITY_POINT_COEFFICIENT = 10;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_prize';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'type_id'], 'required'],
            [['user_id', 'type_id', 'value', 'awarded'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => 'User ID',
            'type_id' => 'Prize Type',
            'value' => 'Prize',
            'awarded' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }


    /**
     * @property int $id
     * @return UserPrize
     */
    public function getPrizeById(int $id = null): UserPrize
    {
        return self::findOne($id);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getPrisesByUser($user): array
    {
        return self::find()->where([
            'user_id' => $user->id,
        ])->all();

    }

    /**
     * @param int $userId
     * @return UserPrize|string
     * @throws \yii\db\Exception
     */
    public function getNewPrize(int $userId = null)
    {
        $userId = $userId ?? Yii::$app->user->id;
        $userPrize = new self();
        $userPrize->user_id = $userId;
        $userPrize->type_id = $this->getRandomPrizeType();
        $prizeModel = $this->getPrizeModel($userPrize->type_id);
        $prizeModel->generatePrize();
        $userPrize->value = $prizeModel->getPrizeValue();

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $userPrize->validate();
            $userPrize->save();
            $userPrize->writeOff();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            return 'Create error' . $e->getMessage();
        }

        return $userPrize;
    }

    /**
     * @return int
     */
    public function getRandomPrizeType(): int
    {
        $variable[] = self::LOYALITY_POINTS_PRIZE;

        if (CashPrize::canGenerate()) {
            $variable[] = self::CASH_PRIZE;
        }

        if (PresentPrize::canGenerate()) {
            $variable[] = self::PRESENT_PRIZE;
        }

        return $variable[mt_rand(0, count($variable) - 1)];
    }

    /**
     * @param $prizeType int
     * @return Prize
     */
    public function getPrizeModel(int $prizeType = null): Prize
    {
        $prizeType = $prizeType ?? $this->getRandomPrizeType();

        switch ($prizeType) {
            case self::CASH_PRIZE:
                return new CashPrize();
            case self::PRESENT_PRIZE:
                return new PresentPrize();
            case self::LOYALITY_POINTS_PRIZE:
                return new LoyalityPointsPrize();
        }
    }

    /**
     * @return Prize
     */
    public function getPrize(): Prize
    {
        $prize = $this->getPrizeModel($this->type_id);
        $prize->setPrize($this->value);
        return $prize;
    }

    /**
     * @return array
     */
    public static function getTypeArray(): array
    {
        return [
            self::CASH_PRIZE => 'cash',
            self::PRESENT_PRIZE => 'present',
            self::LOYALITY_POINTS_PRIZE => 'loyalty points',
        ];
    }

    /**
     * @return string
     */
    public function getConvertButton(): string
    {
        if ($this->type_id == self::CASH_PRIZE && $this->awarded == self::READY_TO_AWARDING) {
            return Html::button('$<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>P', [
                'class' => 'btn btn-warning pull-right btn-xs convert-button',
                'onclick' => "convertCashToPoint($this->id)",
            ]);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getToolButtons(): string
    {
        if ($this->awarded == self::READY_TO_AWARDING) {
            return Html::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>', [
                'class' => 'btn btn-success btn-xs award-button',
                'onclick' => "awardPrize($this->id)",
            ]) . Html::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', [
                'class' => 'btn btn-danger pull-right btn-xs cancel-button',
                'onclick' => "cancelPrize($this->id)",
            ]);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function exchangeMoneyToPoints(): bool
    {
        if ($this->awarded == self::READY_TO_AWARDING && $this->type_id == self::CASH_PRIZE && $this->user_id == Yii::$app->user->id) {
            $this->writeOn();
            $this->value = $this->value * self::EXCHANGE_MONEY_TO_LOYALITY_POINT_COEFFICIENT / 100;
            $this->type_id = self::LOYALITY_POINTS_PRIZE;
            if ($this->save()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getStatusArray(): array
    {
        return [
            self::READY_TO_AWARDING => 'ready to awarding',
            self::IN_PROCESS_AWARDING => 'in progress awarding',
            self::AWARDED => 'awarded',
            self::CANCELED => 'canceled',
        ];
    }

    /**
     * @return string
     */
    public function getStatusName(): string
    {
        $array = self::getStatusArray();

        return $array[$this->awarded];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function prizeToAward(): bool
    {
        $this->awarded = self::IN_PROCESS_AWARDING;

        if($this->type_id == self::LOYALITY_POINTS_PRIZE && $this->save()){
            return $this->userLoyaltyPointReplenishmentTransaction();
        }

        return $this->save();
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function cancelPrize(): bool
    {

        $transaction = Yii::$app->db->beginTransaction();
        $this->awarded = self::CANCELED;
        if($this->save() && $this->writeOn()){
            $transaction->commit();

            return true;
        }

        $transaction->rollback();

        return false;
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function userCashReplenishmentTransaction(): string
    {
        $msg = "Record #$this->id: ";

        if ($this->type_id == self::CASH_PRIZE && $this->awarded == self::IN_PROCESS_AWARDING) {
            $cashBank = new CashInBank();

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $amount = $this->value / 100;
                $cashBank->userAccountReplenishment($this->user_id, $amount);
                $this->awarded = self::AWARDED;
                $this->save();
                $transaction->commit();

                return $msg . "User $this->user_id ===> $ $amount done!";

            } catch (\Exception $e) {

                $transaction->rollback();

                return $msg . "User $this->user_id ===> $ $amount error -> " . $e->getMessage();
            }
        }

        return $msg . 'skip...';
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function userPresentReceiptTransaction(): string
    {
        $msg = "Record #$this->id: ";

        if ($this->type_id == self::PRESENT_PRIZE && $this->awarded == self::IN_PROCESS_AWARDING) {
            $postBox = new UserPostBox();

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $presentId = $this->value;
                $postBox->userPBPresentReceipt($this->user_id, $presentId);
                $this->awarded = self::AWARDED;
                $this->save();
                $transaction->commit();
                $present = $this->getPrize()->getPrize();


                return $msg . "User $this->user_id ===> present $present receipted!";

            } catch (\Exception $e) {

                $transaction->rollback();
                $present = $this->getPrize()->getPrize();

                return $msg . "User $this->user_id ===> present $present error -> " . $e->getMessage();
            }
        }

        return $msg . 'skip...';
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function userLoyaltyPointReplenishmentTransaction(): bool
    {
        if ($this->type_id == self::LOYALITY_POINTS_PRIZE && $this->awarded == self::IN_PROCESS_AWARDING) {
            $loyaltyPoints = new LoyaltyPointsUser();

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $amount = $this->value;
                $loyaltyPoints->userAccountReplenishment($this->user_id, $amount);
                $this->awarded = self::AWARDED;
                $this->save();
                $transaction->commit();

                return true;

            } catch (\Exception $e) {
                $transaction->rollback();
                Yii::error("Loyalty point replenishment error: " . $e->getMessage());
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function writeOff() : bool
    {
        if($this->type_id == self::CASH_PRIZE){
            $amount = $this->value/100;

            return Availables::getAvailableCash()->availableWriteOff($amount);
        }elseif($this->type_id == self::PRESENT_PRIZE){
            return Present::getPresent($this->value)->availableWriteOff();
        }

        return true;
    }

    /**
     * @return bool
     */
    public function writeOn() : bool
    {
        if($this->type_id == self::CASH_PRIZE){
            $amount = $this->value/100;

            return Availables::getAvailableCash()->availableWriteOn($amount);
        }elseif($this->type_id == self::PRESENT_PRIZE){
            return Present::getPresent($this->value)->availableWriteOn();
        }

        return true;
    }
}
