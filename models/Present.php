<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "present".
 *
 * @property int $id
 * @property string $name
 * @property int $available_quantity
 */
class Present extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'present';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['available_quantity'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Name',
            'available_quantity' => 'Available quantity',
        ];
    }

    /**
     * @return array
     */
    public static function getPresentsIds()
    {
        $presents = static::find()->select(['id'])->where(['>', 'available_quantity', 0])->asArray()->all();

        return array_map(function ($present) { return $present['id']; } , $presents);
    }

    /**
     * @param int $id
     * @return self
     */
    public static function getPresent(int $id) : ActiveRecord
    {
        return self::findOne($id);
    }

    /**
     * @return int
     */
    public static function getCountPresents() : int
    {
        return self::find()->sum('available_quantity');
    }

    /**
     * @return bool
     */
    public function availableWriteOff() : bool
    {
        $this->available_quantity --;

        if($this->available_quantity >= 0 && $this->save()){
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function availableWriteOn() : bool
    {
        $this->available_quantity ++;

        if($this->save()){
            return true;
        }

        return false;
    }
}
