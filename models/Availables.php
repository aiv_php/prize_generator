<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "availables".
 *
 * @property int $id
 * @property string $name
 * @property double $available_amount
 */
class Availables extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'availables';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['available_amount'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Name',
            'available_amount' => 'Available Amount',
        ];
    }

    /**
     * @return Availables
     */
    public static function getAvailableCash() : Availables
    {
        return self::findOne(['name' => 'cash']);
    }

    /**
     * @param float $amount
     * @return bool
     */
    public function availableWriteOff(float $amount) :bool
    {
        $this->available_amount -= $amount;
        return $this->save() ? true : false;
    }

    /**
     * @param float $amount
     * @return bool
     */
    public function availableWriteOn(float $amount) :bool
    {
        $this->available_amount += $amount;
        return $this->save() ? true : false;
    }
}
