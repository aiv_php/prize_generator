<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_post_box".
 *
 * @property int $id
 * @property int $user_id
 * @property int $present_id
 * @property int $amount
 *
 * @property Present $present
 * @property User $user
 */
class UserPostBox extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_post_box';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'present_id'], 'required'],
            [['user_id', 'present_id', 'amount'], 'integer'],
            [['present_id'], 'exist', 'skipOnError' => true, 'targetClass' => Present::class, 'targetAttribute' => ['present_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => 'User ID',
            'present_id' => 'Present ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPresent()
    {
        return $this->hasOne(Present::class, ['id' => 'present_id']);
    }

    /**
     * @return string
     */
    public function getPresentName()
    {
        return $this->present->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param int $userId
     * @param int $presentId
     * @return UserPostBox
     */
    public function getUserPBPresent(int $userId, int $presentId): UserPostBox
    {
        $userPBPresent = self::findOne(['user_id' => $userId, 'present_id' => $presentId]);

        if(!$userPBPresent){
            $userPBPresent = new UserPostBox();
            $userPBPresent->user_id = $userId;
            $userPBPresent->present_id = $presentId;
            $userPBPresent->save();
        }

        return $userPBPresent;
    }

    /**
     * @param $userId
     * @return array|null
     */
    public function getPBPresentsForUser($userId)
    {
        $userPBPresents = self::find()->where(['user_id' => $userId])->all();

        return $userPBPresents ? ArrayHelper::map($userPBPresents,'presentName', 'amount') : null;
    }

    /**
     * @return array|null
     */
    public function getMyPBPresentsArray()
    {
        if (!empty(Yii::$app->user)) {
            $iAm = Yii::$app->user->id;
            return $this->getPBPresentsForUser($iAm);
        }
    }

    /**
     * @param int $userId
     * @param int $presentId
     * @return bool
     */
    public function userPBPresentReceipt(int $userId, int $presentId)
    {
        $userPBPresent = $this->getUserPBPresent($userId, $presentId);
        $userPBPresent->amount ++;
        return $userPBPresent->save();
    }
}
