<?php
/**
 * Created by PhpStorm.
 * User: aiv_dev
 * Date: 27.02.18
 * Time: 2:09
 */

namespace app\classes;


use app\models\Availables;
use app\models\Present;
use app\models\UserPrize;

class MyPrize
{
    private $user;
    private $userPrize;

    /**
     * MyPrize constructor.
     */
    private function __construct()
    {
        $this->user = \Yii::$app->user;
        $this->userPrize = new UserPrize();
    }

    /**
     * @return array
     */
    public static function getMyPrizes() : array
    {
        $myPrize = new self();
        return $myPrize->userPrize->getPrisesByUser($myPrize->user);
    }

    /**
     * @param int|null $id
     * @return UserPrize
     * @throws \yii\db\Exception
     */
    public function getMyPrize(int $id = null) : UserPrize
    {
        $myPrize = new self();
        return $id ? $myPrize->userPrize->getPrizeById($id) : $myPrize->userPrize->getNewPrize($myPrize->user->id);
    }

    /**
     * @return string
     */
    public static function getLeftoverPrize() : string
    {
        $countPresents = Present::getCountPresents();
        $cashAmount = Availables::getAvailableCash()->available_amount;
        $loyaltyPoints = 'lots of';
        $echo = 'Leftover: ';
        $echo .= 'presents - ' . $countPresents . ', ';
        $echo .= 'cash - $' . $cashAmount . ', ';
        $echo .= 'loyalty points - ' . $loyaltyPoints . '';
        return $echo;
    }
}