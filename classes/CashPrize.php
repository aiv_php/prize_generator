<?php
namespace app\Classes;

use app\interfaces\Prize;
use app\models\Availables;
use http\Exception;
use Yii;

/**
 * Created by PhpStorm.
 * User: aiv_dev
 * Date: 27.02.18
 * Time: 0:34
 */

class CashPrize implements Prize
{
    const MIN_AMOUNT = 1;
    const MAX_AMOUNT = 10;
    const STEP_NEXT = 0.5;

    private $prizeValue;

    /**
     * @return bool
     */
    public static function canGenerate(): bool
    {
        $available_amount = Availables::getAvailableCash()->available_amount;
        return $available_amount > 0 ? true : false;
    }

    public function generatePrize(): void
    {
        try{
            $variable = range(self::MIN_AMOUNT, self::MAX_AMOUNT, self::STEP_NEXT);
            $this->prizeValue = $variable[mt_rand(0, count($variable)-1)];
            $available_amount = Availables::getAvailableCash()->available_amount;
            $this->prizeValue = $this->prizeValue > $available_amount ? $available_amount : $this->prizeValue;
        }catch (Exception $e){
            Yii::warning('Data error: ' . $e->getMessage());
        }
    }

    /**
     * @return string
     */
    public function getPrizeCongratulations(): string
    {
        return "Congratulations! You received a cash prize of $ $this->prizeValue!";
    }

    /**
     * @return int
     */
    public function getPrizeValue() : int
    {
        return $this->prizeValue * 100;
    }

    /**
     * @return string
     */
    public function getPrize() : string
    {
        return "$ $this->prizeValue";
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return "Cash";
    }

    /**
     * @param int $value
     */
    public function setPrize(int $value) : void
    {
        $this->prizeValue = $value / 100;
    }

}