<?php
namespace app\Classes;
use app\interfaces\Prize;
use http\Exception;
use Yii;

/**
 * Created by PhpStorm.
 * User: aiv_dev
 * Date: 27.02.18
 * Time: 0:34
 */

class LoyalityPointsPrize implements Prize
{
    const MIN_POINTS = 10;
    const MAX_POINTS = 100;
    const STEP_NEXT = 10;

    private $prizeValue;

    /**
     * @return bool
     */
    public function canGenerate(): bool
    {
        return true;
    }

    /**
     *
     */
    public function generatePrize(): void
    {
        try{
            $variable = range(self::MIN_POINTS, self::MAX_POINTS, self::STEP_NEXT);
            $this->prizeValue = $variable[mt_rand(0, count($variable)-1)];
        }catch (Exception $e){
            Yii::warning('Data error: ' . $e->getMessage());
        }
    }

    /**
     * @return string
     */
    public function getPrizeCongratulations(): string
    {
        return "Congratulations! You got a prize of $this->prizeValue bonus points!";
    }

    /**
     * @return int
     */
    public function getPrizeValue() : int
    {
        return $this->prizeValue;
    }

    /**
     * @return string
     */
    public function getPrize() : string
    {
        return "$this->prizeValue points";
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return "Loyalty points";
    }

    /**
     * @param int $value
     */
    public function setPrize(int $value) : void
    {
        $this->prizeValue = $value;
    }

}