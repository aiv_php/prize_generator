<?php

namespace app\classes;

use app\interfaces\Prize;
use app\models\Present;
use http\Exception;
use Yii;

/**
 * Created by PhpStorm.
 * User: aiv_dev
 * Date: 27.02.18
 * Time: 0:34
 */
class PresentPrize implements Prize
{

    private $prizeValue;

    /**
     * @return bool
     */
    public static function canGenerate(): bool
    {
        $enablePresent = Present::find()->where(['>', 'available_quantity', 0])->count();

        return $enablePresent ? true : false;
    }

    public function generatePrize(): void
    {
        try {
            $variable = Present::getPresentsIds();
            $this->prizeValue = $variable[mt_rand(0, count($variable) - 1)];
        } catch (Exception $e) {
            Yii::warning('Data error: ' . $e->getMessage());
        }
    }

    /**
     * @return string
     */
    public function getPrizeCongratulations(): string
    {

        return "Congratulations! You got a prize of " . $this->getPrize() . "!";
    }

    /**
     * @return int
     */
    public function getPrizeValue() : int
    {
        return $this->prizeValue;
    }

    /**
     * @return Present
     */
    public function getPrizeModel(): Present
    {
        return Present::getPresent($this->prizeValue);
    }

    /**
     * @return string
     */
    public function getPrize(): string
    {
        return $this->getPrizeModel()->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return "Present";
    }

    /**
     * @param int $value
     */
    public function setPrize(int $value): void
    {
        $this->prizeValue = $value;
    }

}