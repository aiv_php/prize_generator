<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \app\models\UserPrize;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserPrizeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My prizes list';
?>
<div class="user-prize-index">

    <div class="row">
        <div class="col-lg-8 col-md-6 col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">

                        <?= Html::encode($this->title) ?>

                        <span class="loftover badge pull-right">

                    <?php Pjax::begin(['id' => 'leftoverPrize', 'timeout' => false, 'enablePushState' => false]); ?>

                    <?= \app\classes\MyPrize::getLeftoverPrize() ?>

                    <?php Pjax::end(); ?>

                        </span>
                    </h3>
                </div>
                <div class="panel-body">
                    <p>

                        <?= Html::button('Get my NEW prize!', [
                            'class' => 'btn btn-info',
                            'id' => 'createPrizeButton'
                        ]) ?>

                    </p>
                    <div class="alert alert-success" role="alert" id="alertMsg" hidden></div>
                    <div class="alert alert-error" role="alert" id="alertMsg" hidden></div>

                    <?php Pjax::begin(['id' => 'myPrizesList']); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'contentOptions' => ['style' => 'width:20px;'],
                            ],
                            [
                                'label' => 'Prize type',
                                'value' => function ($model) {
                                    return $model->getPrize()->getType();
                                },
                                'attribute' => 'type_id',
                                'filter' => UserPrize::getTypeArray(),
                                'filterInputOptions' => ['prompt' => 'all types', 'class' => 'form-control', 'id' => 'prizeType'],
                                'headerOptions' => ['style' => 'width:25%'],
                            ],
                            [
                                'label' => 'Prize',
                                'value' => function ($model) {
                                    return $model->getPrize()->getPrize() . $model->getConvertButton();
                                },
                                'format' => 'raw',
                                'headerOptions' => ['style' => 'width:20%'],
                            ],
                            [
                                'value' => function ($model) {
                                    return $model->getStatusName();
                                },
                                'attribute' => 'awarded',
                                'filter' => UserPrize::getStatusArray(),
                                'filterInputOptions' => ['prompt' => 'all statuses', 'class' => 'form-control', 'id' => 'awardedStatus']
                            ],
                            [
//                        'label' => 'Tools',
                                'value' => function ($model) {
                                    return $model->getToolButtons();
                                },
                                'format' => 'raw',
                                'headerOptions' => ['style' => 'width:80px'],
                            ],
                        ]
                    ]); ?>

                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">

            <?php Pjax::begin(['id' => 'myPanels', 'timeout' => false, 'enablePushState' => false]); ?>

            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">My bank account:</h3></div>
                <div class="panel-body">

                    <?= "US Dollars: $" . $bankAccount->getMyCash() . "." ?>

                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">My loyalty points:</h3></div>
                <div class="panel-body">

                    <?= "Loyalty points: " . $loyaltyPointAccount->getMyLoyaltyPoints() . "." ?>

                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">My post box:</h3></div>
                <ul class="list-group">

                    <?php foreach ($postBox->getMyPBPresentsArray() as $item => $number): ?>

                        <li class="list-group-item">

                            <?= $item . ' - ' . $number . 'pcs.' ?>

                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>

            <?php Pjax::end(); ?>

        </div>
    </div>

</div>
