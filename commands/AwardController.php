<?php

namespace app\commands;

use app\models\UserPrize;
use yii\console\Controller;

class AwardController extends Controller
{
    /**
     * This command replenishment cash to users accounts.
     */
    public function actionCashReplenishment()
    {
        $cashPrizeToAward = UserPrize::find()
            ->where([
                'type_id' => UserPrize::CASH_PRIZE,
                'awarded' => UserPrize::IN_PROCESS_AWARDING
            ])
            ->all();
        if (count($cashPrizeToAward) > 0) {
            foreach ($cashPrizeToAward as $prize) {
                echo $prize->userCashReplenishmentTransaction() . "\n";
            }
            echo "\n" . "end."  . "\n";
            return;
        }
        echo "skip."  . "\n";
        return;

    }

    /**
     * This command receipted presents for users.
     */
    public function actionPresentReceipt()
    {
        $presentPrizeToAward = UserPrize::find()
            ->where([
                'type_id' => UserPrize::PRESENT_PRIZE,
                'awarded' => UserPrize::IN_PROCESS_AWARDING
            ])
            ->all();
        if (count($presentPrizeToAward) > 0) {
            foreach ($presentPrizeToAward as $prize) {
                echo $prize->userPresentReceiptTransaction() . "\n";
            }
            echo "\n" . "end."  . "\n";
            return;
        }
        echo "skip."  . "\n";
        return;
    }
}
